#include <fstream>
#include <TROOT.h>
#include <TString.h>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <iostream>
using namespace std;
int main(int argc, char** argv){
  if(argc != 6){
    cout<<"ERROR::Wrong parameters! Aborted."<<endl;
    return 1;
  }

  TString channel = argv[1]; 
  TString cate = argv[2];
  TString prod = argv[3];
  TString logfile = argv[4];
  TString outdir = argv[5];

  cout<<"INFO::Producing average MVA ranking for "<<channel<<" "<<cate<<" "<<prod<<" ..."<<endl;

  TString cmd = "mkdir -pv "+outdir;
  system(cmd.Data());

  vector<TString> vMassName;
  vMassName.push_back("600");
  //if(!(cate=="2j"&&prod=="vbf"))vMassName.push_back("600");
  vMassName.push_back("700");
  vMassName.push_back("800");
  vMassName.push_back("900");
  vMassName.push_back("1000");
  vMassName.push_back("1100");
  vMassName.push_back("1200");
  vMassName.push_back("1300");
  vMassName.push_back("1400");
  vMassName.push_back("1500");
  //vMassName.push_back("1400");
  //vMassName.push_back("1800");
  //vMassName.push_back("2200");
  //vMassName.push_back("2600");
  //vMassName.push_back("3000");

  //declare functions
  map<TString, Double_t> getImp(TString);
  Double_t getAveImp(TString, vector<map<TString, Double_t> >);
  map<UInt_t, TString> getVarInOrder(map<TString, Double_t>);
  void outInOrder(TString, map<UInt_t, TString>, map<TString, Double_t>);

  TString filename;
  TString outfile;

  //ranking over masses
  vector<map<TString, Double_t> > vMImp;
  for(UInt_t i=0;i<vMassName.size();i++){
    TString massname = vMassName[i];
    TString tmp_file = logfile;
    filename = tmp_file.ReplaceAll("MASS",massname);
    vMImp.push_back(getImp(filename));
  }

  //average ranking
  map<TString, Double_t> mAveImp;

  //e.g. first mass
  map<TString, Double_t> mImp_tmp = vMImp[0];

  for(map<TString,Double_t>::iterator itr=mImp_tmp.begin();itr!=mImp_tmp.end();itr++){
    TString varname = itr->first;
    Double_t average = getAveImp(varname, vMImp);
    mAveImp[varname] = average;
  }

  //get var in order
  map<UInt_t, TString> mVarInOrder = getVarInOrder(mAveImp);

  //output into txt file(in order)
  outfile = outdir+"/"+channel+"_"+cate+"_"+prod+".txt";
  outInOrder(outfile, mVarInOrder, mAveImp);

}//ENd

map<TString, Double_t> getImp(TString file){
  ifstream fin;
  fin.open(file);
  if(!fin.is_open()) cout<<"Failed to open "<<file<<endl;

  //declare map<var, imp>
  map<TString, Double_t> mImp;

  //read in
  string line;
  TString Line;
  string word;
  while(getline(fin,line)){
    Line = line;
    if(!Line.Contains("Importance")) continue;
    getline(fin,line);//skip the first line of ----------------
    while(getline(fin,line)){
      Line = line;
      //if(Line.Contains("----------------------------------------")) break;
      if(Line.Contains("--------------------")) break;
      //get maps
      stringstream ss;
      ss<<line;
      for(int i=0;i<6;i++){
        ss>>word; 
      } //skip 5 words to get variable name
      TString varname = word;
      for(int i=0;i<2;i++){
        ss>>word;
      } //skip 1 words to get importance
      TString impname = word;
      Double_t imp = impname.Atof();
      mImp[varname] = imp;
    }
  }
  return mImp;
}

Double_t getAveImp(TString varname, vector<map<TString, Double_t> > vMImp){
  Double_t average;
  for(UInt_t i=0;i<vMImp.size();i++){
    average += vMImp[i][varname];
  }
  average = average/vMImp.size();
  return average;
}

map<UInt_t, TString> getVarInOrder(map<TString, Double_t> mAveImp){
  map<UInt_t, TString> mVarInOrder;

  for(map<TString,Double_t>::iterator itr=mAveImp.begin();itr!=mAveImp.end();itr++){
    TString varname = itr->first;
    Double_t imp = itr->second;
    UInt_t order = 1;

    for(map<TString,Double_t>::iterator itr2=mAveImp.begin();itr2!=mAveImp.end();itr2++){
      if(itr2->second > imp) order++;
    }
    mVarInOrder[order] = varname;
  }
  
  return mVarInOrder;
}

void outInOrder(TString outfile, map<UInt_t, TString> mVarInOrder, map<TString, Double_t> mAveImp){
  ofstream fout(outfile);
  if(!fout.is_open()) cout<<"Error::Can't create "<<outfile<<endl;

  for(map<UInt_t, TString>::iterator itr=mVarInOrder.begin();itr!=mVarInOrder.end();itr++){
    TString varname = itr->second;
    Double_t imp = mAveImp[varname];
    Double_t percent = imp/mAveImp[mVarInOrder[1]]*100;
    TString percent_str = Form("%.0f",percent);
    fout<<varname<<"\t"<<TString(Form("%.3f",imp))<<"\t"<<percent_str<<"%"<<endl;
  }
}

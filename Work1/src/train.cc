//#include <fstream>
#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TMVA/Factory.h>
#include <TCut.h>
#include <iostream>
#include <map>
using namespace std;
int main(int argc, char** argv){
  if(argc != 7){
    cout<<"ERROR::Wrong parameters! Aborted."<<endl;
    return 1;
  }

  TString mass = argv[1]; 
  TString channel = argv[2]; 
  TString cate = argv[3];
  TString prod = argv[4];
  TString indir = argv[5];
  TString outdir = argv[6];

  cout<<"INFO::Testing and training for "<<prod<<" "<<mass<<"GeV "<<channel<<" "<<cate<<" ..."<<endl;
  TString filepath = indir;
  TFile* outfile = TFile::Open(outdir+"/TMVA.root","RECREATE");

  TString sname[7] = {"Signal_","H125","WW","Top","Zjets","Diboson","Wjets"};
  sname[0] = sname[0] + prod;
  vector<TString> v_sample;
  for(int i=0;i<7;i++){
    v_sample.push_back(sname[i]);
  }

  map<TString, TFile*> m_file;
  map<TString, TTree*> m_train; 
  map<TString, TTree*> m_test; 

  for(UInt_t i=0;i<v_sample.size();i++){
    TString sample = v_sample[i];
    if(sample=="Signal_"+prod) m_file[sample] = TFile::Open(filepath+"/"+sample+"/"+mass+".root","READ");
    else m_file[sample] = TFile::Open(filepath+"/"+sample+".root","READ");
    m_train[sample] = (TTree*)m_file[sample]->Get("miniNT_"+channel+"_"+cate);
    m_test[sample] = (TTree*)m_file[sample]->Get("miniNT_"+channel+"_"+cate);
  }

  //
  TMVA::Factory* factory = new TMVA::Factory("factory", outfile, "");
  double sigGlobalEventWeight = 1.0;
  double bkgGlobalEventWeight = 1.0;
  //training
  for(map<TString, TTree*>::iterator itr=m_train.begin();itr!=m_train.end();itr++){
    TString sample = itr->first;
    if(sample=="Signal_"+prod){ 
      factory->AddSignalTree(m_train[sample], sigGlobalEventWeight, TMVA::Types::kTraining);
      factory->AddSignalTree(m_test[sample], sigGlobalEventWeight, TMVA::Types::kTesting);
    }
    else{ 
      factory->AddBackgroundTree(m_train[sample], bkgGlobalEventWeight, TMVA::Types::kTraining);
      factory->AddBackgroundTree(m_test[sample], bkgGlobalEventWeight, TMVA::Types::kTesting);
    }
  }

  //Event weight
  factory->SetWeightExpression("EW");

  factory->AddVariable("LeadPt", 'F');
  factory->AddVariable("SubPt", 'F');
  factory->AddVariable("MET", 'F');
  factory->AddVariable("MPT", 'F');
  factory->AddVariable("METRel", 'F');
  factory->AddVariable("MPTRel", 'F');
  factory->AddVariable("Ptll", 'F');
  factory->AddVariable("Mll", 'F');
  factory->AddVariable("DEtall", 'F');
  if(cate == "2j"){
    factory->AddVariable("Pttot", 'F');
    factory->AddVariable("Mjj", 'F');
    factory->AddVariable("DYjj", 'F');
  }

  TCut cut = "";
  factory->PrepareTrainingAndTestTree( cut, "SplitMode=random:!V" );

  factory->BookMethod( TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20" );

  factory->TrainAllMethods();
  factory->TestAllMethods();
  factory->EvaluateAllMethods();    

  outfile->Close();

  delete factory;

  return 1;
}//ENd

#include <fstream>
#include <iostream>
#include <algorithm>
#include <TROOT.h>
#include <TStyle.h>
#include <TString.h>
#include <TH2.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <utility>
using namespace std;
int main(int argc, char** argv){
  if(argc != 4){
    cout<<"ERROR::Wrong parameters! Aborted."<<endl;
    return 1;
  }

  TString channel = argv[1]; 
  TString cate = argv[2];
  TString prod = argv[3];

  vector<TString> vMassName;
  vMassName.push_back("600");
  //if(!(cate=="2j"&&prod=="vbf"))vMassName.push_back("600");
  vMassName.push_back("700");
  vMassName.push_back("800");
  vMassName.push_back("900");
  vMassName.push_back("1000");
  vMassName.push_back("1100");
  vMassName.push_back("1200");
  vMassName.push_back("1300");
  vMassName.push_back("1400");
  vMassName.push_back("1500");
  //vMassName.push_back("1400");
  //vMassName.push_back("1800");
  //vMassName.push_back("2200");
  //vMassName.push_back("2600");
  //vMassName.push_back("3000");

  cout<<"INFO::Producing correlation maxtrix plots for "<<channel<<" "<<cate<<" "<<prod<<" ..."<<endl;
  
  TString indir = "./result";
  TString outdir = "./result/correlation/"+channel+"_"+cate+"_"+prod;

  TString cmd = "mkdir -pv "+outdir;
  system(cmd.Data());

  void drawCor(TString, TString, vector<TString>, TString, TString,TString);

  drawCor(indir, outdir, vMassName, channel, cate,prod);
}
void drawCor(TString indir, TString outdir, vector<TString> v_mass, TString channel, TString cate,TString prod){
  Double_t level[] = {-100,-90,-80,80,90,100};
  Int_t color[] = {2,4,9,4,2};
  gStyle->SetPalette(sizeof(color)/sizeof(Int_t),color);


  //bkg
  for(UInt_t i=0;i<1;i++){
    TString mass = v_mass[i];
    TString filename = indir+"/"+mass+"_"+channel+"_"+cate+"_"+prod+"/TMVA.root";
    TFile *f = new TFile(filename);
    TH2F* h_bkg = (TH2F*)f->Get("CorrelationMatrixB");
    h_bkg->SetTitle((TString)(h_bkg->GetTitle()));
    TCanvas* c= new TCanvas("c","c",20,10,700,500);
    h_bkg->SetContour(sizeof(level)/sizeof(Double_t), level);
    h_bkg->Draw("textCOLZ");
    c->Print(outdir+"/cor_bkg.png");
  }
  
  //sig
  TH2F* h_sig;
  for(UInt_t i=0;i<v_mass.size();i++){
    TString mass = v_mass[i];
    TString filename = indir+"/"+mass+"_"+channel+"_"+cate+"_"+prod+"/TMVA.root";
    TFile *f = new TFile(filename);
    TH2F* h = (TH2F*)f->Get("CorrelationMatrixS");
    if(i==0){
      h_sig = h;
    }else{
      h_sig->Add(h);
    }
  }
  for(int i=0;i<h_sig->GetNbinsX();i++)
    for(int j=0;j<h_sig->GetNbinsY();j++){
      h_sig->SetBinContent(i+1,j+1,int(h_sig->GetBinContent(i+1,j+1)/v_mass.size()));
  }
  h_sig->SetTitle((TString)(h_sig->GetTitle()));
  TCanvas* c= new TCanvas("c","c",20,10,700,500);
  h_sig->SetContour(sizeof(level)/sizeof(Double_t), level);
  h_sig->Draw("textCOLZ");
  c->Print(outdir+"/cor_sig.png");
  c->Clear();
}

#include <TString.h>
#include <TChain.h>
#include <TROOT.h>
#include <TH1F.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMath.h>
#include <map>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;
int main(int argc, char** argv){
  if(argc != 6){
    cout<<"ERROR::Wrong parameters! Aborted."<<endl;
    return 1;
  }

  TString channel = argv[1]; 
  TString cate = argv[2];
  TString prod = argv[3];
  TString logfile = argv[4];
  TString outdir = argv[5];

  cout<<"INFO::Drawing MVA ranking for "<<channel<<" "<<cate<<" "<<prod<<" ..."<<endl;

  TString cmd = "mkdir -pv "+outdir;
  system(cmd.Data());

  vector<TString> vMassName;
  vMassName.push_back("600");
  vMassName.push_back("700");
  vMassName.push_back("800");
  vMassName.push_back("900");
  vMassName.push_back("1000");
  vMassName.push_back("1100");
  vMassName.push_back("1200");
  vMassName.push_back("1300");
  vMassName.push_back("1400");
  vMassName.push_back("1500");
  //vMassName.push_back("1400");
  //vMassName.push_back("1800");
  //vMassName.push_back("2200");
  //vMassName.push_back("2600");
  //vMassName.push_back("3000");

  //declare functions
  map<TString, Double_t> getImp(TString);
  //map<UInt_t, TString> getVarInOrder(map<TString, Double_t>);
  void drawRank(map<TString, TGraph*>, TString, TString);

  TString filename;

  //ranking over masses
  map<UInt_t, map<TString, Double_t> > mMImp;
  for(UInt_t i=0;i<vMassName.size();i++){
    TString massname = vMassName[i];
    TString tmp_file = logfile;
    filename = tmp_file.ReplaceAll("MASS",massname);
    UInt_t mass = massname.Atoi();
    mMImp[mass] = getImp(filename);
  }

  //get var in order
  //map<TString, map<UInt, TString> > mMVarInOrder;
  //for(map<TString, map<UInt_t, TString> >::iterator itr=mMImp.begin();itr!=mMImp.end();itr++){
  //  TString mass = itr->first;
  //  map<UInt_t, TString> m_imp = itr->second;
  //  mMVarInOrder[mass] = getVarInOrder(m_imp);
  //}

  //
  map<TString, Int_t> m_color;
  m_color["DEtall"] 	= 2;
  m_color["Mll"] 	= 3;
  m_color["LeadPt"] 	= 4;
  m_color["SubPt"] 	= 5;
  m_color["Ptll"] 	= 6;
  m_color["Pttot"] 	= 7;
  m_color["DYjj"] 	= 8;
  m_color["Mjj"] 	= 9;
  m_color["MET"] 	= 40;
  m_color["MPT"] 	= 41;
  m_color["METRel"] 	= 42;
  m_color["MPTRel"] 	= 43;
  //m_color["CJV"] 	= 46;
  //m_color["OLV"] 	= 49;

  //TGraph
  map<TString, TGraph*> mGr; //<var, gr>
  map<UInt_t, map<TString, Double_t> >::iterator itr_tmp = mMImp.begin();
  map<TString, Double_t> m_tmp = itr_tmp->second;
  for(map<TString, Double_t>::iterator itr=m_tmp.begin();itr!=m_tmp.end();itr++){
    TString varname = itr->first;
    vector<Double_t> X;
    vector<Double_t> Y;
    for(map<UInt_t, map<TString, Double_t> >::iterator itr2 = mMImp.begin();itr2!=mMImp.end();itr2++){
      UInt_t massvalue = itr2->first;
      Double_t impvalue;
      map<TString, Double_t> m_imp = itr2->second;
      for(map<TString, Double_t>::iterator itr3=m_imp.begin();itr3!=m_imp.end();itr3++){
        if(itr3->first != varname) continue;
        impvalue = itr3->second;
      }
      X.push_back(massvalue);
      Y.push_back(impvalue);
    }
    TGraph* gr = new TGraph(X.size(),&(X[0]),&(Y[0]));
    gr->SetLineColor(m_color[varname]);
    gr->SetMarkerColor(m_color[varname]);
    gr->SetMarkerStyle(21);
    gr->GetHistogram()->SetLabelSize(0.03,"X");
    gr->GetHistogram()->SetLabelSize(0.03,"Y");
    mGr[varname] = gr;
  }

  //output
  TString title = "BDT ranking, "+channel+" "+cate+" "+prod;
  TString outfile = outdir+"/"+channel+"_"+cate+"_"+prod+".png";
  drawRank(mGr, title, outfile);

}//ENd

map<TString, Double_t> getImp(TString file){
  ifstream fin;
  fin.open(file);
  if(!fin.is_open()) cout<<"Failed to open "<<file<<endl;

  //declare map<var, imp>
  map<TString, Double_t> mImp;

  //read in
  string line;
  TString Line;
  string word;
  while(getline(fin,line)){
    Line = line;
    if(!Line.Contains("Importance")) continue;
    getline(fin,line);//skip the first line of ----------------
    while(getline(fin,line)){
      Line = line;
      //if(Line.Contains("----------------------------------------")) break;
      if(Line.Contains("--------------------")) break;
      //get maps
      stringstream ss;
      ss<<line;
      for(int i=0;i<6;i++){
        ss>>word; 
      } //skip 5 words to get variable name
      TString varname = word;
      for(int i=0;i<2;i++){
        ss>>word;
      } //skip 1 words to get importance
      TString impname = word;
      Double_t imp = impname.Atof();
      mImp[varname] = imp;
    }
  }
  return mImp;
}
//map<UInt_t, TString> getVarInOrder(map<TString, Double_t> mAveImp){
//  map<UInt_t, TString> mVarInOrder;
//
//  for(map<TString,Double_t>::iterator itr=mAveImp.begin();itr!=mAveImp.end();itr++){
//    TString varname = itr->first;
//    Double_t imp = itr->second;
//    UInt_t order = 1;
//
//    for(map<TString,Double_t>::iterator itr2=mAveImp.begin();itr2!=mAveImp.end();itr2++){
//      if(itr2->second > imp) order++;
//    }
//    mVarInOrder[order] = varname;
//  }
//  
//  return mVarInOrder;
//}
//
void drawRank(map<TString, TGraph*> mGr, TString title, TString outfile){
  TCanvas* can = new TCanvas(title,"",20,10,700,500);
  gPad->SetGrid(1,1);
  gPad->SetTicky();
  TLegend* leg = new TLegend(0.9,0.35,1.0,0.9);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->SetFillColor(0);
  Double_t maxY = 0;
  Double_t minY = 1000;
  for(map<TString,TGraph*>::iterator itr=mGr.begin();itr!=mGr.end();itr++){
    Double_t max_y = itr->second->GetHistogram()->GetMaximum();
    Double_t min_y = itr->second->GetHistogram()->GetMinimum();
    if(maxY < max_y) maxY = max_y;
    if(minY > min_y) minY = min_y;
  }
  
  for(map<TString,TGraph*>::iterator itr=mGr.begin();itr!=mGr.end();itr++){
    TString varname = itr->first;
    TGraph* gr = itr->second;
    gr->SetTitle(title);
    gr->GetYaxis()->SetTitle("BDT ranking value");
    gr->GetXaxis()->SetTitle("MASS");
    if(itr==mGr.begin()){ 
      gr->SetMaximum(1.1*maxY);
      gr->SetMinimum(0.9*minY);
      gr->Draw("ALP");
    } else gr->Draw("LP");
    leg->AddEntry(gr,varname,"lp");
  }
  leg->Draw();
  can->Print(outfile);
}

#!/bin/bash
mass=$1
ch=$2
cat=$3
prod=$4

do_bsub=1

masses=(600 700 800 900 1000 1100 1200 1300 1400 1500)
channel=(emme)
cate=(0j)
prods=(ggf)

WD=$(pwd)

cd src
make
cd -

if [ $# -eq 4 ]&&[ -f src/train ]; then 
  indir=${WD}/../Work0/MiniNT
  outdir=${WD}/result/${mass}_${ch}_${cat}_${prod}
  mkdir -pv result/${mass}_${ch}_${cat}_${prod}
  cd ${WD}/result/${mass}_${ch}_${cat}_${prod}
  ln -sf ../../src/train .

  echo Processing ${mass} ${ch} ${cat} ${prod}
  rm -rf LSFJOB_*
  if [ $do_bsub -eq 1 ];then
    bsub -q 1nd train ${mass} ${ch} ${cat} ${prod} ${indir} ${outdir}
  else
    ./train ${mass} ${ch} ${cat} ${prod} ${indir} ${outdir}
  fi
elif [ $# -eq 1 ]&&[ $1 = all ];then
  for m in ${masses[*]}
  do
  for c in ${channel[*]}
  do
  for j in ${cate[*]}
  do
  for p in ${prods[*]}
  do
    echo Processing ${m} ${c} ${j} ${p}
    mkdir -pv ${WD}/result/${m}_${c}_${j}_${p}
    cd ${WD}/result/${m}_${c}_${j}_${p}
    ln -sf ../../src/train .
    indir=${WD}/../Work0/MiniNT
    outdir=${WD}/result/${m}_${c}_${j}_${p}
    rm -rf LSFJOB_*
    if [ $do_bsub -eq 1 ];then
      bsub -q 1nd train ${m} ${c} ${j} ${p} ${indir} ${outdir}
    else
      ./train ${m} ${c} ${j} ${p} ${indir} ${outdir}
    fi
  done
  done
  done
  done
else 
  echo Wrong parameters!
fi

cd $WD

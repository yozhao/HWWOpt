#!/bin/bash
ch=$1
cat=$2
prod=$3

channel=(emme)
cate=(0j)
prods=(ggf)

WD=$(pwd)
g++ -o src/get_avecor src/get_avecor.cc `root-config --cflags --glibs`
if [ $# -eq 3 ]; then 
  echo Processing ${ch} ${cat} ${prod}
  ./src/get_avecor ${ch} ${cat} ${prod}
elif [ $# -eq 1 ]&&[ $1 = all ];then
  for c in ${channel[*]}
  do
  for j in ${cate[*]}
  do
  for p in ${prods[*]}
  do

    #not ready
    #if [ $c = mm -a $j = 2j ]; then
    #  continue
    #fi

    echo Processing ${c} ${j} ${p}
    ./src/get_avecor ${c} ${j} ${p}
  done
  done
  done
else 
  echo Wrong parameters!
fi

cd $WD

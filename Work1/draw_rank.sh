#!/bin/bash
ch=$1
cat=$2
prod=$3

channel=(emme)
cate=(0j)
prods=(ggf)

WD=$(pwd)
outdir=result/rankingPlots
if [ $# -eq 3 ]; then 
  echo Processing ${ch} ${cat} ${prod}
  g++ -o src/draw_rank src/draw_rank.cc `root-config --cflags --glibs`
  logfile=result/MASS_${ch}_${cat}_${prod}/STDOUT
  ./src/draw_rank ${ch} ${cat} ${prod} ${logfile} ${outdir}
elif [ $# -eq 1 ]&&[ $1 = all ];then
  for c in ${channel[*]}
  do
  for j in ${cate[*]}
  do
  for p in ${prods[*]}
  do

    #not ready
    #if [ $c = mm -a $j = 2j ]; then
    #  continue
    #fi

    echo Processing ${c} ${j} ${p}
    g++ -o src/draw_rank src/draw_rank.cc `root-config --cflags --glibs`
    logfile=result/MASS_${c}_${j}_${p}/STDOUT
    ./src/draw_rank ${c} ${j} ${p} ${logfile} ${outdir}
  done
  done
  done
else 
  echo Wrong parameters!
fi

cd $WD

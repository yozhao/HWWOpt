#include "../include/Util.h"

bool Util::IsFinished(const TString filename = ""){
  bool finished = 0;
  ifstream fin(filename);
  if(!fin.is_open()){
    cout<<"ERROR:: "<<filename<<" not found!"<<endl;
  }
  string word;
  fin >> word;
  if(word == "1") finished = 1;
  fin.close();
  return finished;
}

TString Util::Pwd(){
  //For ubuntu, NAME_MAX is 255, PATH_MAX is 4096; here taken to be 500
  char buf[500];
  int max = 500;
  TString pwd = getcwd(buf,max);
  return pwd;
}

vector<TString> Util::GetName(const TString filename, const TString keyname){
  vector<TString> vValueName;
  TString samplename;
  ifstream fin(filename);
  if(!fin.is_open()){
    cout<<"ERROR:: "<<filename<<" not found!"<<endl;
  }
  string line, theline;
  while(getline(fin, line)){
    TString Line = line;
    if(!Line.BeginsWith(keyname)) continue;
    theline = line;
  }
  stringstream ss;
  ss<<theline;
  string word;
  bool is_value = 0;
  while(ss >> word){
    samplename = word;
    if((!is_value)&&samplename.Contains(":")){ 
      is_value = 1;
      continue;
    }else if(!is_value){
      continue;
    }
    vValueName.push_back(samplename);
  }
  fin.close();
  return vValueName;
}

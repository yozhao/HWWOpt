#include <TString.h>
#include <TChain.h>
#include <TROOT.h>
#include <TH1F.h>
#include <TAxis.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TMath.h>
#include <map>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;
void drawOpt(TString WPName, TString StepName, TString workdir, vector<TString> v_mass, TString channel, TString cate, TString prod){

  cout<<"INFO::Running "<<channel<<" "<<cate<<" "<<prod<<" ..."<<endl;

  //config
  TString cfgfile = workdir + "/config/"+WPName+"/"+StepName+"/"+channel+"_"+cate+"_"+prod+".cfg";

  //declare
  void getCuts(TString, map<TString, Double_t>&, map<TString, vector<Double_t> >&); //basic, scan
  void getValues(TString, vector<TString>, TString, TString, TString, map<TString, Double_t>, map<TString, vector<Double_t> >, map<TString, map<TString, vector<Double_t> > >&, map<TString, map<TString, vector<Double_t> > >&, map<TString, map<TString, vector<pair<Double_t, Double_t> > > >&, map<TString, map<TString, vector<pair<Double_t, Double_t> > > >&);

  map<TString, Double_t> mBasic;
  map<TString, vector<Double_t> > mScan;
  getCuts(cfgfile, mBasic, mScan);

  map<TString, map<TString, vector<Double_t> > > mSigEff;
  map<TString, map<TString, vector<Double_t> > > mBkgEff;
  map<TString, map<TString, vector<pair<Double_t, Double_t> > > > mSgn;
  map<TString, map<TString, vector<pair<Double_t, Double_t> > > > mRSS;
  getValues(workdir, v_mass, channel, cate, prod, mBasic, mScan, mSigEff, mBkgEff, mSgn, mRSS);

  //draw
  void drawPlot(TGraphErrors*, TGraphErrors*, TGraphErrors*, TGraphErrors*, TString, TString, TString, TString, TString);

  for(map<TString, map<TString, vector<pair<Double_t, Double_t> > > >::iterator itr=mRSS.begin();itr!=mRSS.end();itr++){
    TString varname = itr->first;
    map<TString, vector<pair<Double_t, Double_t> > > m_rss = itr->second;
    for(map<TString, vector<pair<Double_t, Double_t> > >::iterator itr2=m_rss.begin();itr2!=m_rss.end();itr2++){
      TString mass = itr2->first;

      vector<Double_t> vx, vy_rss, vey_rss, vy_sgn, vey_sgn, vy_sigeff, vy_bkgeff;
      for(UInt_t i=0;i<mScan[varname].size();i++){
        vx.push_back(mScan[varname][i]);
        vy_rss.push_back(mRSS[varname][mass][i].first);
        vey_rss.push_back(mRSS[varname][mass][i].second);
        vy_sgn.push_back(mSgn[varname][mass][i].first);
        vey_sgn.push_back(mSgn[varname][mass][i].second);
        vy_sigeff.push_back(mSigEff[varname][mass][i]);
        vy_bkgeff.push_back(mBkgEff[varname][mass][i]);
        //cout<<"DEBUG:: Var "<<varname<<" mass "<<mass<<" rss "<<vy_rss[i]<<" err_rss "<<vey_rss[i]<<endl;
      }
    
      TGraphErrors* gr_rss = new TGraphErrors(vx.size(),&(vx[0]), &(vy_rss[0]), 0, &(vey_rss[0]));
      TGraphErrors* gr_sgn = new TGraphErrors(vx.size(),&(vx[0]), &(vy_sgn[0]), 0, &(vey_sgn[0]));
      TGraphErrors* gr_sigeff = new TGraphErrors(vx.size(),&(vx[0]), &(vy_sigeff[0]), 0, 0);
      TGraphErrors* gr_bkgeff = new TGraphErrors(vx.size(),&(vx[0]), &(vy_bkgeff[0]), 0, 0);
      
      //set styles
      UInt_t colors[] = {1,4,2,6};
      UInt_t markers[] = {21,20,22, 25,24,26};
      gr_rss->SetLineColor(colors[2]);
      gr_rss->SetMarkerColor(colors[2]);
      gr_rss->SetMarkerStyle(markers[0]);
      gr_rss->GetHistogram()->SetLabelSize(0.05,"X");
      gr_rss->GetHistogram()->SetLabelSize(0.05,"Y");
      gr_rss->GetHistogram()->SetTitleSize(0.05,"X");
      gr_rss->GetHistogram()->SetTitleSize(0.05,"Y");
      gr_rss->GetHistogram()->SetTitleOffset(1.,"X");
      gr_rss->GetHistogram()->SetTitleOffset(1.,"Y");
      gr_sgn->SetLineColor(colors[3]);
      gr_sgn->SetMarkerColor(colors[3]);
      gr_sgn->SetMarkerStyle(markers[0]);
      gr_sigeff->SetLineColor(colors[1]);
      gr_sigeff->SetMarkerColor(colors[1]);
      gr_sigeff->SetMarkerStyle(markers[0]);
      gr_bkgeff->SetLineColor(colors[0]);
      gr_bkgeff->SetMarkerColor(colors[0]);
      gr_bkgeff->SetMarkerStyle(markers[0]);

      TString outdir = workdir + "/out/"+WPName+"/"+StepName;
      TString outname = mass+"_"+channel+"_"+cate+"_"+prod;
      TString title = prod+" H"+mass+" "+channel+" "+cate;
      drawPlot(gr_rss,gr_sgn,gr_sigeff,gr_bkgeff,outdir,outname, title, varname, mass);
    }
  }
}//ENd
void getCuts(TString cfgfile, map<TString, Double_t>& m_basic, map<TString, vector<Double_t> >& m_scan){
  ifstream fin;
  fin.open(cfgfile);
  if(!fin.is_open()) {cout<<"ERROR::Didn't find config file: "<<cfgfile<<"!"<<endl;}
  string line;
  TString KeyName;
  TString ValName[5];
  while(getline(fin,line)){
    TString Line = line;
    if(Line.BeginsWith("#")) continue;
    stringstream ss;
    ss<<line;
    ss>>KeyName;
    for(int t=0;t<5;t++) ss>>ValName[t];
    Int_t order = ValName[0].Atoi();
    Double_t fixvalue, step, r1, r2;
    if(order == 2){
      fixvalue = ValName[1].Atof();
      m_basic[KeyName] = fixvalue;
    }else if(order == 1 || order == 0){
      if(order ==1){
        fixvalue = ValName[1].Atof();
        m_basic[KeyName] = fixvalue;
      }
      step = ValName[2].Atof();
      r1 = ValName[3].Atof();
      r2 = ValName[4].Atof();
      vector<Double_t> v_cuts;
      if(step==0) cout<<"ERROR::Step of scanning on "<<KeyName<<" being 0."<<endl;
      else{
        for(int i=0;i<=(r2-r1)/step;i++){
          Double_t cutvalue = r1 + i*step;
          v_cuts.push_back(cutvalue);
        }
      }
      m_scan[KeyName] = v_cuts;
    }else continue;
  }
  fin.close();
}
//
void getValues(TString wd, vector<TString> v_mass, TString channel, TString cat, TString prod, map<TString, Double_t> m_basic, map<TString, vector<Double_t> > m_scan, map<TString, map<TString, vector<Double_t> > >& m_sigeff, map<TString, map<TString, vector<Double_t> > >& m_bkgeff, map<TString, map<TString, vector<pair<Double_t, Double_t> > > >& m_sgn, map<TString, map<TString, vector<pair<Double_t, Double_t> > > >& m_rss){

  TString sname_sig = "Signal_"+prod;
  TString sname_bkg[6] = {"H125","WW","Top","Diboson","Zjets","Wjets"};

  vector<TString> fname_sig;
  vector<TString> fname_bkg;
  for(UInt_t i=0;i<v_mass.size();i++){
    fname_sig.push_back(wd + "/../Work0/MiniNT/" + sname_sig + "/" + v_mass[i] + ".root");
  }
  for(int i=0;i<6;i++){
    fname_bkg.push_back(wd + "/../Work0/MiniNT/" + sname_bkg[i] + ".root");
  }

  map<TString, TChain*> m_chain;
  for(UInt_t i=0;i<v_mass.size();i++){
    TString massname = v_mass[i];
    TChain* ch = new TChain("miniNT_"+channel+"_"+cat);
    ch->Add(fname_sig[i]);
    m_chain[massname] = ch;
  }
  TChain* ch_bkg = new TChain("miniNT_"+channel+"_"+cat);
  for(UInt_t i=0;i<fname_bkg.size();i++){
    ch_bkg->Add(fname_bkg[i]);
  }
  m_chain["Bkg"] = ch_bkg;

  //declare
  pair<Double_t, Double_t> getSgn(TH1F*, TH1F*);
  pair<Double_t, Double_t> getRSS(TH1F*, TH1F*);

  //prepare to calculate
  map<TString, map<TString, vector<pair<Double_t, Double_t> > > > m_ys;	//<var<mass<cut-<yb,ya>>>>
  map<TString, map<TString, vector<pair<Double_t, Double_t> > > > m_yb;
  map<TString, map<TString, vector<TH1F*> > > m_hS;			//<var<mass<cut-<h>>>
  map<TString, map<TString, vector<TH1F*> > > m_hB;

  //get X in irregular binning
  //regular binning
  vector<Double_t> getRegularHX(Int_t,Double_t,Double_t);

  //initialize
  for(map<TString, vector<Double_t> >::iterator itr=m_scan.begin();itr!=m_scan.end();itr++){
    TString varname = itr->first;
    vector<Double_t> v_cut = itr->second;
    for(UInt_t i=0;i<v_mass.size();i++){
      TString mass = v_mass[i];

      Double_t r_max = 2*mass.Atof();
      vector<Double_t> v_x = getRegularHX(30,0,r_max);

      vector<pair<Double_t, Double_t> > v_ys;
      vector<TH1F*> v_hS;
      for(UInt_t j=0;j<v_cut.size();j++){
        v_ys.push_back(make_pair(0,0));
        //
        TString cutname = Form("%.2f",v_cut[j]);
        TString hname = "hMT_Sig_"+mass+"_"+channel+"_"+cat+"_"+prod+"_cut"+varname+"_"+cutname;
        TH1F* hMT = new TH1F(hname,hname,v_x.size()-1,&(v_x[0]));
        hMT->Sumw2();
        v_hS.push_back(hMT);
      }
      m_ys[varname][mass] = v_ys;
      m_hS[varname][mass] = v_hS;
      //
      vector<pair<Double_t, Double_t> > v_yb;
      vector<TH1F*> v_hB;
      for(UInt_t i=0;i<v_cut.size();i++){
        v_yb.push_back(make_pair(0,0));
        //
        TString cutname = Form("%.2f",v_cut[i]);
        TString hname = "hMT_Bkg_"+mass+"_"+channel+"_"+cat+"_"+prod+"_cut"+varname+"_"+cutname;
        TH1F* hMT = new TH1F(hname,hname,v_x.size()-1,&(v_x[0]));
        hMT->Sumw2();
        v_hB.push_back(hMT);
      }
      m_yb[varname][mass] = v_yb;
      m_hB[varname][mass] = v_hB;
    }
  }

  //declare variables
  UInt_t BJet;
  Bool_t OLV, CJV;
  Float_t MT, EW, Mll,DEtall,LeadPt,SubPt,MET,MPT,METRel,MPTRel,Ptll,Pttot,Mjj,DYjj;
  //set address
  for(map<TString, TChain*>::iterator itr=m_chain.begin();itr!=m_chain.end();itr++){
    TString massname = itr->first;
    TChain* chain = itr->second;
    chain->SetBranchAddress("MT", &MT);
    chain->SetBranchAddress("EW", &EW);
    chain->SetBranchAddress("LeadPt", &LeadPt);
    chain->SetBranchAddress("SubPt", &SubPt);
    chain->SetBranchAddress("MET", &MET);
    chain->SetBranchAddress("MPT", &MPT);
    chain->SetBranchAddress("METRel", &METRel);
    chain->SetBranchAddress("MPTRel", &MPTRel);
    chain->SetBranchAddress("Ptll", &Ptll);
    chain->SetBranchAddress("Mll", &Mll);
    chain->SetBranchAddress("DEtall", &DEtall);
    if(cat!="0j"){ 
      chain->SetBranchAddress("BJet", &BJet);
      chain->SetBranchAddress("Pttot", &Pttot);
      chain->SetBranchAddress("OLV", &OLV);
      chain->SetBranchAddress("CJV", &CJV);
      chain->SetBranchAddress("Mjj", &Mjj);
      chain->SetBranchAddress("DYjj", &DYjj);
    }
   
    bool doTest = 0;
    for(Long_t en=0;en<(doTest?1000:chain->GetEntries());en++){
      chain->GetEntry(en);
      Double_t weight = EW;
      for(map<TString,vector<Double_t> >::iterator itr_scan=m_scan.begin();itr_scan!=m_scan.end();itr_scan++){
        TString varname = itr_scan->first;
        vector<Double_t> v_cuts = itr_scan->second;
        bool c_basic = 1;
        for(map<TString, Double_t>::iterator itr_b=m_basic.begin();itr_b!=m_basic.end();itr_b++){
          TString cutname = itr_b->first;
          if(cutname==varname) continue; //skip the scanning variable in the basic cuts
          Double_t cutvalue = itr_b->second;
          if(cutname=="CJV") c_basic = c_basic && (cutvalue==2?1:(CJV == cutvalue));
          else if(cutname=="OLV") c_basic = c_basic && (cutvalue==2?1:(OLV == cutvalue)); 
          else if(cutname=="LeadPt") c_basic = c_basic && LeadPt > cutvalue; 
          else if(cutname=="SubPt") c_basic = c_basic && SubPt > cutvalue; 
          else if(cutname=="MET") c_basic = c_basic && MET > cutvalue; 
          else if(cutname=="MPT") c_basic = c_basic && MPT > cutvalue; 
          else if(cutname=="METRel") c_basic = c_basic && METRel > cutvalue; 
          else if(cutname=="MPTRel") c_basic = c_basic && MPTRel > cutvalue; 
          else if(cutname=="Ptll") c_basic = c_basic && Ptll > cutvalue; 
          else if(cutname=="Pttot") c_basic = c_basic && Pttot < cutvalue; 
          else if(cutname=="Mll") c_basic = c_basic && Mll > cutvalue; 
          else if(cutname=="DEtall") c_basic = c_basic && DEtall < cutvalue; 
          else if(cutname=="Mjj") c_basic = c_basic && Mjj > cutvalue; 
          else if(cutname=="DYjj") c_basic = c_basic && DYjj > cutvalue; 
          else cout<<"ERROR::Didn't find cut expression code for "<<cutname<<"! Omitting it."<<endl;
        }
        if(cat!="0j") c_basic = c_basic && BJet == 0; 
        if(!c_basic) continue;
        for(UInt_t i=0;i<v_cuts.size();i++){
          //before cut
          if(massname!="Bkg"){
            m_ys[varname][massname][i].first += weight; 
          }else{
            for(UInt_t m=0;m<v_mass.size();m++){
              TString mass = v_mass[m];
              m_yb[varname][mass][i].first += weight; 
            }
          }
          //after cut
          Double_t cutvalue = v_cuts[i];
          //apply cut
          bool c_cut = 1;
          if(varname=="CJV") c_cut = c_cut && (cutvalue==2?1:(CJV == cutvalue)); 
          else if(varname=="OLV") c_cut = c_cut && (cutvalue==2?1:(OLV==cutvalue)); 
          else if(varname=="LeadPt") c_cut = c_cut && LeadPt > cutvalue; 
          else if(varname=="SubPt") c_cut = c_cut && SubPt > cutvalue; 
          else if(varname=="MET") c_cut = c_cut && MET > cutvalue; 
          else if(varname=="MPT") c_cut = c_cut && MPT > cutvalue; 
          else if(varname=="METRel") c_cut = c_cut && METRel > cutvalue; 
          else if(varname=="MPTRel") c_cut = c_cut && MPTRel > cutvalue; 
          else if(varname=="Ptll") c_cut = c_cut && Ptll > cutvalue; 
          else if(varname=="Pttot") c_cut = c_cut && Pttot < cutvalue; 
          else if(varname=="Mll") c_cut = c_cut && Mll > cutvalue; 
          else if(varname=="DEtall") c_cut = c_cut && DEtall < cutvalue; 
          else if(varname=="Mjj") c_cut = c_cut && Mjj > cutvalue; 
          else if(varname=="DYjj") c_cut = c_cut && DYjj > cutvalue; 
          else cout<<"ERROR::Didn't find cut expression code for "<<varname<<"! Omitting it."<<endl;
          if(!c_cut) continue;
          //ya
          if(massname != "Bkg"){
            m_ys[varname][massname][i].second += weight;
          }else{
            for(UInt_t m=0;m<v_mass.size();m++){
              TString mass = v_mass[m];
              m_yb[varname][mass][i].second += weight;
            }
          }
          //hMT
          if(massname != "Bkg"){
              m_hS[varname][massname][i]->Fill(MT,weight);
          }else{
            for(UInt_t m=0;m<v_mass.size();m++){
              TString mass = v_mass[m];
              m_hB[varname][mass][i]->Fill(MT,weight);
            }
          }
        }//cut scan
      }//opt variables
    }//entries
  }//chains: 600, 700, ..., Bkg

  //calculate
  for(map<TString, vector<Double_t> >::iterator itr=m_scan.begin();itr!=m_scan.end();itr++){
    TString varname = itr->first;
    vector<Double_t> v_cut = itr->second;
    for(UInt_t i=0;i<v_mass.size();i++){
      TString mass = v_mass[i];

      vector<Double_t> v_sigeff;
      vector<pair<Double_t, Double_t> > v_sgn, v_rss;

      for(UInt_t j=0;j<v_cut.size();j++){
        Double_t yb = m_ys[varname][mass][j].first;
        Double_t ya = m_ys[varname][mass][j].second;
        v_sigeff.push_back(yb==0?0:ya/yb);
        
        TH1F* h_sig = m_hS[varname][mass][j];
        TH1F* h_bkg = m_hB[varname][mass][j];
        v_sgn.push_back(getSgn(h_sig, h_bkg));
        v_rss.push_back(getRSS(h_sig, h_bkg));
      }
      m_sigeff[varname][mass] = v_sigeff;
      m_sgn[varname][mass] = v_sgn;
      m_rss[varname][mass] = v_rss;
      //
      vector<Double_t> v_bkgeff;
      for(UInt_t i=0;i<v_cut.size();i++){
        Double_t yb = m_yb[varname][mass][i].first;
        Double_t ya = m_yb[varname][mass][i].second;
        v_bkgeff.push_back(yb==0?0:ya/yb);
      }
      m_bkgeff[varname][mass] = v_bkgeff;
    }
  }
}//End
pair<Double_t, Double_t> getSgn(TH1F* hS, TH1F* hB){
  Double_t sgn, e_sgn;

  Double_t GetSgn(Double_t, Double_t);
  Double_t GetSgnErr(Double_t, Double_t, Double_t, Double_t);

  Double_t S = hS->Integral(0,hS->GetNbinsX()+1);
  Double_t B = hB->Integral(0,hB->GetNbinsX()+1);
  Double_t eS = 0;
  Double_t eB = 0;
  for(int i=0;i<hS->GetNbinsX()+1;i++){
    eS += pow(hS->GetBinError(i+1),2);
    eB += pow(hB->GetBinError(i+1),2);
  }
  eS = sqrt(eS);
  eB = sqrt(eB);

  sgn = GetSgn(S,B);
  e_sgn = GetSgnErr(S,eS,B,eB);
  pair<Double_t, Double_t> p_sgn = make_pair(sgn, e_sgn);
  return p_sgn;
}
pair<Double_t, Double_t> getRSS(TH1F* hS, TH1F* hB){
  Double_t rss = 0;
  Double_t e_rss = 0;
  
  Double_t GetSgn(Double_t, Double_t);
  Double_t GetSgnErr(Double_t, Double_t, Double_t, Double_t);

  for(int i=0;i<hS->GetNbinsX();i++){
    Double_t S = hS->GetBinContent(i+1);
    Double_t B = hB->GetBinContent(i+1);
    Double_t eS = hS->GetBinError(i+1);
    Double_t eB = hB->GetBinError(i+1);
      //include overflow
    if(i==hS->GetNbinsX()-1){
      S += hS->GetBinContent(i+2);
      B += hB->GetBinContent(i+2);
      eS = sqrt(pow(eS,2)+pow(hS->GetBinError(i+2),2));
      eB = sqrt(pow(eB,2)+pow(hB->GetBinError(i+2),2));
    }
    Double_t sgn = GetSgn(S,B);
    Double_t e_sgn = GetSgnErr(S,eS,B,eB);
    rss += pow(sgn,2);
    e_rss += pow(sgn*e_sgn,2);
  }
  rss = sqrt(rss);
  e_rss = rss==0?0:sqrt(e_rss)/rss;
  pair<Double_t, Double_t> p_rss = make_pair(rss, e_rss);
  return p_rss;
}
Double_t GetSgn(Double_t S = 0, Double_t B = 0){
  if(B<=0 || S<=0) return 0;
  else{
    Double_t t = (S+B)*log(1+S/B)-S;
    if(t>0) return sqrt(2*t);
    else return 0;
  }
}
Double_t GetSgnErr(Double_t S = 0, Double_t eS = 0, Double_t B = 0, Double_t eB = 0){
  if(B<=0 || S<=0) return 0;
  else{
    Double_t ds = eS;
    Double_t db = eB;
    Double_t t = (S+B)*log(1+S/B)-S;
    if(t>0) return sqrt(pow((log(1+S/B)/sqrt(2*t))*ds,2)+pow(((log(1+S/B)-S/B)/sqrt(2*t))*db,2));
    else return 0;
  }
}
void drawPlot(TGraphErrors* gr_rss, TGraphErrors* gr_sgn, TGraphErrors* gr_sigeff, TGraphErrors* gr_bkgeff, TString outdir, TString outname, TString title, TString varname, TString massname){

  TString cmd = "mkdir -pv " + outdir;
  system(cmd.Data());

  TCanvas* can = new TCanvas("c_"+varname+"_"+outname,"",20,10,700,500); 
  can->SetTickx();
  can->SetTicky();
  can->SetGrid(1,1);

  Double_t width = gr_rss->GetX()[1] - gr_rss->GetX()[0];
  TString yname = "";
  TString xname; 
  if(varname=="DEtall"||varname=="DYjj") xname = varname+Form("/%.2f",width);
  else if(varname=="CJV"||varname=="OLV") xname = varname+"[2: cut not applied]";
  else xname = varname+Form("/%.0fGeV",width);
  gr_rss->SetTitle(title);
  gr_rss->GetYaxis()->SetTitle(yname);
  gr_rss->GetXaxis()->SetTitle(xname);
  gr_rss->SetMaximum(1.1);
  gr_rss->SetMinimum(0);

  //scale Y axis
  Double_t max_rss = 0;
  Double_t max_cut = 0;
  for(int b_g=0;b_g<gr_rss->GetN();b_g++){
    Double_t b_rss = gr_rss->GetY()[b_g];
    if(max_rss < b_rss) {max_rss = b_rss;max_cut = gr_rss->GetX()[b_g];}
  }
  Double_t yscale_rss = 1./max_rss;
  Double_t yscale_sgn = yscale_rss;
  for(int b_g=0;b_g<gr_rss->GetN();b_g++){
    gr_rss->GetY()[b_g] *= yscale_rss;
    gr_rss->GetEY()[b_g] *= yscale_rss;
  }
  for(int b_g=0;b_g<gr_sgn->GetN();b_g++){
    gr_sgn->GetY()[b_g] *= yscale_sgn;
    gr_sgn->GetEY()[b_g] *= yscale_sgn;
  }
  TString yscale_rss_str = Form("%.2f",yscale_rss);
  TString yscale_sgn_str = Form("%.2f",yscale_sgn);

  gr_rss->Draw("ACP");
  gr_sgn->Draw("CP");
  gr_sigeff->Draw("CP");
  gr_bkgeff->Draw("CP");

  //legend
  TLegend* leg = new TLegend(0.65,0.5,0.90,0.90);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->SetFillColor(0);
  leg->AddEntry(gr_rss,"RSS[x"+yscale_rss_str+"]","lp");
  leg->AddEntry(gr_sgn,"Sgn[x"+yscale_sgn_str+"]","lp");
  leg->AddEntry(gr_sigeff,"Sig efficiency","lp");
  leg->AddEntry(gr_bkgeff,"Bkg efficiency","lp");
  leg->SetTextSize(0.035);
  leg->Draw();

  //tex
  Double_t ymax = gr_rss->GetYaxis()->GetXmax();
  Double_t ymin = gr_rss->GetYaxis()->GetXmin();
  Double_t xmax = gr_rss->GetXaxis()->GetXmax();
  Double_t xmin = gr_rss->GetXaxis()->GetXmin();
  Double_t tX_rss = xmin+0.6*(xmax-xmin);
  Double_t tX_cut = tX_rss;
  Double_t tY_rss = 0.4;
  Double_t tY_cut = 0.3;
  TLatex* tex_rss = new TLatex(tX_rss,tY_rss,Form("Max RSS: %.2f",max_rss));
  TLatex* tex_cut = new TLatex(tX_cut,tY_cut,Form("Ref. cut: > %.2f",max_cut));
  tex_rss->SetTextSize(0.04);
  tex_cut->SetTextSize(0.04);
  tex_rss->Draw();
  tex_cut->Draw();
  can->Print(outdir+"/"+outname+"_"+varname+".png");
}//
/* //not used now!
map<TString, vector<vector<Double_t> > > getHX(TString wd, TString channel, TString cat, TString prod, map<TString,Double_t> m_basic, map<TString,vector<Double_t> > m_scan, Int_t nbins, Double_t x_min, Double_t x_max){
  map<TString, vector<vector<Double_t> > > m_HX;

  TString sname_bkg[6] = {"H125","WW","Top","Diboson","Zjets","Wjets"};
  vector<TString> fname_bkg;
  for(int i=0;i<6;i++){
    fname_bkg.push_back(wd + "/Work0/MiniNT/" + sname_bkg[i] + ".root");
  }
  TChain* ch_bkg = new TChain("miniNT_"+channel+"_"+cat);
  for(UInt_t i=0;i<fname_bkg.size();i++){
    ch_bkg->Add(fname_bkg[i]);
  }

  //declare variables
  UInt_t BJet;
  Bool_t OLV, CJV;
  Float_t MT, EW, Mll,DEtall,LeadPt,SubPt,MET,MPT,METRel,MPTRel,Ptll,Pttot,Mjj,DYjj;
  //set address
  ch_bkg->SetBranchAddress("MT", &MT);
  ch_bkg->SetBranchAddress("EW", &EW);
  ch_bkg->SetBranchAddress("LeadPt", &LeadPt);
  ch_bkg->SetBranchAddress("SubPt", &SubPt);
  ch_bkg->SetBranchAddress("MET", &MET);
  ch_bkg->SetBranchAddress("MPT", &MPT);
  ch_bkg->SetBranchAddress("METRel", &METRel);
  ch_bkg->SetBranchAddress("MPTRel", &MPTRel);
  ch_bkg->SetBranchAddress("Ptll", &Ptll);
  ch_bkg->SetBranchAddress("Mll", &Mll);
  ch_bkg->SetBranchAddress("DEtall", &DEtall);
  if(cat!="0j"){ 
    ch_bkg->SetBranchAddress("BJet", &BJet);
    ch_bkg->SetBranchAddress("Pttot", &Pttot);
    ch_bkg->SetBranchAddress("OLV", &OLV);
    ch_bkg->SetBranchAddress("CJV", &CJV);
    ch_bkg->SetBranchAddress("Mjj", &Mjj);
    ch_bkg->SetBranchAddress("DYjj", &DYjj);
  }

  //prepare to get v_HX
  map<TString, vector<map<Double_t, vector<Double_t> > > > m_m_event; //<varname,< cut-<value, <weight> > > >

  //initialize m_m_event
  for(map<TString,vector<Double_t> >::iterator itr=m_scan.begin();itr!=m_scan.end();itr++){
    TString varname = itr->first;
    vector<Double_t> v_cuts = itr->second;
    vector<map<Double_t, vector<Double_t> > > v_m_event;
    for(UInt_t i=0;i<v_cuts.size();i++){
      map<Double_t, vector<Double_t> > m_event;
      v_m_event.push_back(m_event);
    }
    m_m_event[varname] = v_m_event;
  }

  bool doTest = 0;
//cout<<"DEBUG of calculating v_x:"<<endl;
  for(Long_t en=0;en<(doTest?20000:ch_bkg->GetEntries());en++){
    ch_bkg->GetEntry(en);
//cout<<"EN: "<<en<<endl;
    Double_t weight = EW;
    for(map<TString,vector<Double_t> >::iterator itr=m_scan.begin();itr!=m_scan.end();itr++){
      TString varname = itr->first;
//cout<<"varname: "<<varname<<endl;
      vector<Double_t> v_cuts = itr->second;
      bool c_basic = 1;
      for(map<TString, Double_t>::iterator itr_b=m_basic.begin();itr_b!=m_basic.end();itr_b++){
        TString cutname = itr_b->first;
//cout<<"basic cutname: "<<cutname<<endl;
        if(cutname==varname) continue; //skip the scanning variable in the basic cuts
        Double_t cutvalue = itr_b->second;
        if(cutname=="CJV") c_basic = c_basic && cutvalue==2?1:(CJV == cutvalue);
        else if(cutname=="OLV") c_basic = c_basic && cutvalue==2?1:(OLV == cutvalue); 
        else if(cutname=="LeadPt") c_basic = c_basic && LeadPt > cutvalue; 
        else if(cutname=="SubPt") c_basic = c_basic && SubPt > cutvalue; 
        else if(cutname=="MET") c_basic = c_basic && MET > cutvalue; 
        else if(cutname=="MPT") c_basic = c_basic && MPT > cutvalue; 
        else if(cutname=="METRel") c_basic = c_basic && METRel > cutvalue; 
        else if(cutname=="MPTRel") c_basic = c_basic && MPTRel > cutvalue; 
        else if(cutname=="Ptll") c_basic = c_basic && Ptll > cutvalue; 
        else if(cutname=="Pttot") c_basic = c_basic && Pttot < cutvalue; 
        else if(cutname=="Mll") c_basic = c_basic && Mll > cutvalue; 
        else if(cutname=="DEtall") c_basic = c_basic && DEtall < cutvalue; 
        else if(cutname=="Mjj") c_basic = c_basic && Mjj > cutvalue; 
        else if(cutname=="DYjj") c_basic = c_basic && DYjj > cutvalue; 
        else cout<<"ERROR::Didn't find cut expression code for "<<cutname<<"! Omitting it."<<endl;
      }
      if(cat!="0j") c_basic = c_basic && BJet == 0; 
      if(!c_basic) continue;
      for(UInt_t i=0;i<v_cuts.size();i++){
        Double_t cutvalue = v_cuts[i];
//cout<<"scan cut value: "<<cutvalue<<endl;
        //apply cut
        bool c_cut = 1;
        if(varname=="CJV") c_cut = c_cut && cutvalue==2?1:(CJV == cutvalue); 
        else if(varname=="OLV") c_cut = c_cut && OLV == cutvalue==2?1:(OLV==cutvalue); 
        else if(varname=="LeadPt") c_cut = c_cut && LeadPt > cutvalue; 
        else if(varname=="SubPt") c_cut = c_cut && SubPt > cutvalue; 
        else if(varname=="MET") c_cut = c_cut && MET > cutvalue; 
        else if(varname=="MPT") c_cut = c_cut && MPT > cutvalue; 
        else if(varname=="METRel") c_cut = c_cut && METRel > cutvalue; 
        else if(varname=="MPTRel") c_cut = c_cut && MPTRel > cutvalue; 
        else if(varname=="Ptll") c_cut = c_cut && Ptll > cutvalue; 
        else if(varname=="Pttot") c_cut = c_cut && Pttot < cutvalue; 
        else if(varname=="Mll") c_cut = c_cut && Mll > cutvalue; 
        else if(varname=="DEtall") c_cut = c_cut && DEtall < cutvalue; 
        else if(varname=="Mjj") c_cut = c_cut && Mjj > cutvalue; 
        else if(varname=="DYjj") c_cut = c_cut && DYjj > cutvalue; 
        else cout<<"ERROR::Didn't find cut expression code for "<<varname<<"! Omitting it."<<endl;
        if(!c_cut) continue;

//cout<<"I've passed!"<<endl;
        //get m_event pointer from m_m_event
        map<Double_t, vector<Double_t> > m_event = m_m_event[varname][i];

        //check range
        if(MT<x_min) continue;
        if(m_event.find(MT)!=m_event.end()){
          m_event[MT].push_back(weight);
        }else{ 
          vector<Double_t> v_w;
          v_w.push_back(weight);
          m_event[MT] = v_w;
        }

        m_m_event[varname][i] = m_event;
      }//cut scan
    }//variable loop
  }//entry loop

  //Calculate
  for(map<TString,vector<Double_t> >::iterator itr=m_scan.begin();itr!=m_scan.end();itr++){
    TString varname = itr->first;
    vector<Double_t> v_cuts = itr->second;

    vector<vector<Double_t> > v_x_s;
    for(UInt_t s=0;s<v_cuts.size();s++){
      vector<Double_t> v_x;

      map<Double_t, vector<Double_t> > m_event = m_m_event[varname][s];

      //average bin content
      Double_t yields = 0;
      for(map<Double_t, vector<Double_t> >::iterator itr_e=m_event.begin();itr_e!=m_event.end();itr_e++){
        vector<Double_t> v_weights = itr_e->second;
        for(UInt_t i=0;i<v_weights.size();i++){
          yields += v_weights[i];
        }
      }
      Double_t bin_content = yields/nbins;
      //cout<<"DEBUG::Yields:"<<yields<<" Bin number:"<<nbins<<", average: "<<bin_content<<endl;

      //calculate v_HX
      Double_t bin_yields = 0;
      v_x.push_back(x_min);
      for(map<Double_t, vector<Double_t> >::iterator itr_e=m_event.begin();itr_e!=m_event.end();itr_e++){
        map<Double_t, vector<Double_t> >::iterator itr_e_next = itr_e;
        if(itr_e!=m_event.end()) itr_e_next++;
        Double_t value = itr_e->first;
        Double_t value_next = itr_e_next->first;
        vector<Double_t> v_weights = itr_e->second;
        for(UInt_t i=0;i<v_weights.size();i++){
          bin_yields += v_weights[i];
          if(bin_yields <= bin_content) continue;
          bin_yields -= bin_content;
          //cout<<"DEBUG:: bin_yields "<<bin_yields<<" : bin content "<< bin_content<<endl;
          v_x.push_back(value_next);
        }
      }
      v_x.pop_back(); //reset max - MT of the last event
      if(x_max!=-1) v_x.push_back(x_max);
      else{
        for(map<Double_t, vector<Double_t> >::iterator itr_e=m_event.end();itr_e!=m_event.begin();itr_e--){
          if(itr_e == m_event.end()) continue;
          v_x.push_back(itr_e->first+1);//max: MT of the last event + 1, e.g. 2500 -> 2501, to include 2500
          break;
        }
      }

      //protection when cut too tight
      if(yields==0){
        for(int i=0;i<nbins;i++){
          v_x.push_back(i*120);
        }
      }

      //check X axis
      bool good_x = 1;
      for(UInt_t i=1;i<v_x.size();i++){
        if(v_x[i]==v_x[i-1]){ 
          cout<<"ERROR::Wrongly defined X axis.";
          cout<<"Please check your axis:"<<endl;
          good_x = 0;
          break;
        }
      }
      if(!good_x){
        for(UInt_t i=0;i<v_x.size();i++){
          cout<<v_x[i]<<endl;
        }
      }
      v_x_s.push_back(v_x);
    }
    m_HX[varname] = v_x_s;
  }
  return m_HX;
}//
*/
vector<Double_t> getRegularHX(Int_t nbins, Double_t min, Double_t max){
  vector<Double_t> v_hx;
  for(int j=0;j<=nbins;j++){
    v_hx.push_back(min+j*(max-min)/nbins);
  }
  return v_hx;
}

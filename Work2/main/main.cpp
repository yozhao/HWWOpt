#include <vector>
#include <TROOT.h>
#include <TString.h>
#include "../src/Util.cxx"
#include "../src/DrawOpt.cxx"

int main(int argc, char** argv){

  bool doWP1 = 0; //draw (RSS + eff_sig + eff_bkg) v.s. var
  bool doWP2 = 1; //draw (RSS + eff_sig + eff_bkg) v.s. var, with cuts from WP1 applied

  int ite_index = 2; //Serial number of the iteration

  bool dominant = 1;

  TString WorkDir = Util::Pwd() + "/..";
  TString basic_cfg_name = "../config/basic.cfg";
  vector<TString> vMass= Util::GetName(basic_cfg_name, "Mass");
  vector<TString> vChannel= Util::GetName(basic_cfg_name, "Channel");
  vector<TString> vCate= Util::GetName(basic_cfg_name, "Cate");
  vector<TString> vProd= Util::GetName(basic_cfg_name, "Prod");

  for(UInt_t i=0;i<vChannel.size();i++){
    TString ch = vChannel[i];
    for(UInt_t j=0;j<vCate.size();j++){
      TString cat = vCate[j];
      for(UInt_t k=0;k<vProd.size();k++){
        TString prod = vProd[k];
        if(dominant){
          if(prod=="ggf"&&cat=="2j") continue;
          if(prod=="vbf"&&cat=="0j") continue;
          if(prod=="vbf"&&cat=="1j") continue;
        }
        if(doWP1){ 
          drawOpt("WP1","",WorkDir, vMass, ch, cat,prod);
        }
        if(doWP2){ 
		  TString ite_index_str = Form("%i",ite_index);
          drawOpt("WP2",ite_index_str,WorkDir, vMass, ch, cat,prod);
        }
      }
    }
  }
  return 1;
}
//END

#ifndef __Util__
#define __Util__

#include <TString.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "stdlib.h"
#include "unistd.h"

using namespace std;

class Util {

  private:

  public:

  static bool IsFinished(const TString filename);
  static TString Pwd();
  static vector<TString> GetName(const TString filename, const TString keyname);

};
#endif
